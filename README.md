# Emacs Setup

This is where I'm going to put all the information on my emacs setup and what steps are required for installation. This setup assumes that you're using the _.emacs.d_ from my dotfiles that are available [here](https://gitlab.com/byitkc/dotfiles).

## Initialization

First, we are going to create a symbolic link to the dotfile _emacs.d_ from your home directory to my dotfiles project. In my case, the project is cloned to `~/git/dotfiles`.

```bash
ln -s ~/git/dotfiles/emacs.d ~/.emacs.d
```

## Installation

This uses just the base emacs installation. Install version 36 with the package manager of your choice.

```bash
sudo pacman -S emacs
```

Also, I'm not using the graphical interface for Emacs because I'm a 70 year old Unix administrator with a ponytail. If you are not using my dotfiles, add the following to your `~/.bashrc` to always run emacs in your active terminal.

```config
alias emacs='emacs -nw'
```

## Emacs Setup

Now, it's time to run Emacs from your favorite terminal emaulator by simply typing `emacs`. If this is your first time in Emacs, I recommend going through the tutorial by pressing `C-h t` (`Ctrl+h`, then `t`). Otherwise, we'll jump right in using standard shortcut naming (`C` = CTRL, `M` = META/ALT).

__Sanity Check:__ You did setup your `.emacs.d` directory like we talked about earlier, yeah?

### Initializing Packages Database

If this the your first time running Emacs and using MELPA (The package repo configured for emacs based on my configuration) you will need to list the packages to ensure that we initialize the connection to MELPA before trying to look for a package.

```
M-x package-list-packages <RET>
```

### Installing Packages

We will install all required packages

```
M-x package-install <RET> jedi <RET>
```

### Initializing Packages

#### Initializing Jedi

Please ensure that you have python installed and in your path before we start.

```
M-x jedi:install-server <RET>
```

You may get errors the first time through this. Most likely refering to a missing python or system package. Resolve those errors before continuing on.
